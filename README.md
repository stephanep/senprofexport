# senprofexport

Scripts to convert Scenari XML MCQ quiz files to senprof MCQ question files.

## Package content 

/ : python conversion scripts


## Basic install

The script may be tweaked to run on other OS but it has only be tested on Linux :

    git clone "https://framagit.org/stephanep/amcexport" scenari2amc
    sudo apt install python3-lxml python3-soupsieve python3-bs4

Other package may be required


## Basic usage

topoze2senprof.py in out

* in : directory with Scenari .quiz files (already existing files)
* out : directory to write converted files to (should not exists)

