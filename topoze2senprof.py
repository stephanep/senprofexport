#!/usr/bin/python3
# encoding: utf-8
'''
topoze2senprof

@author:     Stéphane Poinsart
@copyright:  Université de Technologie de Compiègne, Cellule d'Appui Pédagogique
@license:    GPL/LGPL/CECL/MPL
@contact:    stephane.poinsart@utc.fr
'''

import sys
import os
import shutil
import argparse
from pprint import pprint
from bs4 import BeautifulSoup
from pathlib import Path



parser = argparse.ArgumentParser(description='Convert Scenari files from topoze quiz files to senprof question files')
parser.add_argument('inputdir', help='Directory that contains Topoze quiz files (files must exist)')
parser.add_argument('outputdir', help='Directory to write converted files to (should not already exist)')
options = parser.parse_args()

if (not os.path.isdir(options.inputdir)):
    sys.stderr.write('Error: "'+options.inputdir+'" inputdir is not a directory\n')
    sys.exit(1)
inputdir = os.path.realpath(options.inputdir)

outputdir = os.path.realpath(options.outputdir)

shutil.copytree(inputdir, outputdir)
os.chdir(outputdir)


def parsefile(path):
    with open(path) as fp:
        # read the file
        doc=BeautifulSoup(fp, "xml")
        root=doc.find('sc:item')

        # purge all namespace declaration
        REMOVE_ATTRIBUTES = ["xmlns:sc","xmlns:cp","xmlns:sp","xmlns:op"]
        for attribute in REMOVE_ATTRIBUTES:
            for tag in doc.find_all(attrs={attribute: True}):
                del tag[attribute]

        # remake declaration on root item only
        root["xmlns:sc"]="http://www.utc.fr/ics/scenari/v3/core"
        root["xmlns:cp"]="canope.fr:canoprof"
        root["xmlns:sp"]="http://www.utc.fr/ics/scenari/v3/primitive"
        root["xmlns:op"]="utc.fr:ics/opale3"  # remove this namespace after fixing everything

        ### NOW THE REAL CHANGES BEGIN ! EDIT THIS PART !

        # Simple tag rename. Just change this list if you need more
        tagreplace = {
                'op:mcqMur': 'cp:qcm',
                'op:mcqSur': 'cp:qcu',
                'op:exeM': 'cp:questionM',
                'op:txt': 'cp:txt',
                'op:res': 'cp:flow',
                'op:urlM': 'cp:link',
                }
        for src, dest in tagreplace.items():
            for tag in doc.select(src.replace(':', '|')):
                prefix, name = dest.split(':')
                tag.prefix=prefix;
                tag.name=name;

        # Senprof have more nested elements than topoze
        # Change "sc:question" to "sc:question -> cp:block -> sp:body
        for question in doc.select('sc|question'):
            newtag = doc.new_tag('block', nsprefix='cp')
            newtagsub = doc.new_tag('body', nsprefix='sp')

            # and move every child of sc:question to the newly created tag
            for child in question.find_all(recursive=False):
                ext=child.extract()
                newtagsub.append(ext)
            newtag.append(newtagsub)
            question.append(newtag)

        ### ALL CHANGES TO XML DONE

        # write back the file
        with open(str(path).replace('.quiz','.question'), 'w') as fpout:
            fpout.write(str(doc))
    os.remove(path)


for path in Path(".").rglob('*.quiz'):
    print(path)
    parsefile(path)

